#ifndef LCD_H
#define LCD_H

#include "lcdcmd.h"

struct LCD {
    unsigned int CMD_PORT;
    unsigned int RS;
    unsigned int RW;
    unsigned int E;

    unsigned int DATA_PORT;
    unsigned int DATA_PINS;
};

void LCD_4bit_init(const struct LCD *lcd);

inline void LCD_8bit_send_cmd(const struct LCD *lcd, char cmd);
inline void LCD_4bit_send_cmd(const struct LCD *lcd, char cmd);
inline void LCD_4bit_write(const struct LCD *lcd, char data);

void LCD_format_print(const struct LCD *lcd, unsigned int flgs, const char* string, ...);

#endif // LCD_H
