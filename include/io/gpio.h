#ifndef IO_GPIO_H
#define IO_GPIO_H

#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"


#define PIN0 GPIO_PIN_0
#define PIN1 GPIO_PIN_1
#define PIN2 GPIO_PIN_2
#define PIN3 GPIO_PIN_3
#define PIN4 GPIO_PIN_4
#define PIN5 GPIO_PIN_5
#define PIN6 GPIO_PIN_6
#define PIN7 GPIO_PIN_7


#define GPIO_GET(GPIO_PORT_BASE, PINS) \
    (HWREG(GPIO_PORT_BASE + (GPIO_O_DATA + ((PINS) << 2))))

#define GPIO_ON(GPIO_PORT_BASE, PINS) \
            (HWREG(GPIO_PORT_BASE + (GPIO_O_DATA + ((PINS) << 2))) = (PINS))

#define GPIO_OFF(GPIO_PORT_BASE, PINS) \
            (HWREG(GPIO_PORT_BASE + (GPIO_O_DATA + ((PINS) << 2))) = 0)

#define GPIO_TOGGLE(GPIO_PORT_BASE, PINS) \
            ((HWREG(GPIO_PORT_BASE + (GPIO_O_DATA + ((PINS) << 2)))) ^= (PINS))
			

#endif // IO_GPIO_H
