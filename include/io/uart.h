#ifndef IO_UART_H
#define IO_UART_H

struct UART {
    unsigned int periph;
	unsigned int port;
	unsigned int rx_pin;
	unsigned int tx_pin;
	unsigned int base;
};

void UART_init(struct UART *uart, unsigned int ulBaud);

int UART_write(struct UART *uart, unsigned char* string, int len);
int UART_send(struct UART *uart, unsigned char* string);
int UART_printf(const struct UART *uart, const char *pString, ...);

unsigned int UART_get(struct UART *uart, unsigned char* buffer, unsigned char stop, unsigned int buffer_size);

#endif // IO_UART_H
