#ifndef IOUTILS_H
#define IOUTILS_H

#include "stdarg.h"

struct data {
    char* data;
    unsigned int size;
};

const char* i_to_str(unsigned int number, char base, char isSigned);
const char* int_to_str(int number);
const char* uint_to_str(unsigned int number);

// return data struct. If data.size == 0, data.data is null term C string
const struct data* format_parse(const char** pString, va_list args);

#endif // IOUTILS_H
