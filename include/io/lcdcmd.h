#ifndef LCDCMD_H
#define LCDCMD_H

// data mode
#define MODE_8BIT       0x30
#define MODE_4BIT       0x20
#define MODE_5X8        0x20
#define MODE_5X10       0x24
#define MODE_1STR       0x20
#define MODE_2STR       0x28

// display options
#define DISPLAY_DISABLE         0x08
#define DISPLAY_ENABLE          0x0C
#define DISPLAY_CURSOR_BLINK    0x09
#define DISPLAY_CURSOR          0x0A

// shift
#define SHIFT_NO    0x04
#define SHIFT_DEC   0x05
#define SHIFT_INC   0x06

// move
#define MOVE_CURSOR_LEFT    0x10
#define MOVE_CURSOR_RIGHT   0x14
#define MOVE_DISPLAY_LEFT   0x18
#define MOVE_DISPLAY_RIGHT  0x1C

// line
#define LINE_1   0x80
#define LINE_2   0xC0


#endif // LCDCMD_H
