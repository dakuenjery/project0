
# Microcontroller properties.
PART = LM3S328
CPU = -mcpu=cortex-m3
DRIVER = cm3
#FPU=-mfpu=fpv4-sp-d16 -mfloat-abi=softfp

# Paths
PREFIX_ARM = ../Stellaris/gcc-arm-none-eabi-4_7-2013q1/bin/arm-none-eabi
STELLARISWARE_PATH = ../Stellaris/StellarisWare
INC_PATH = include
SRC_PATH = src
SRC_DIRS = . io
BIN_PATH = bin

# Project name (W/O .c extension eg. "main")
PROJECT_NAME = project1
# Linker file name
LINKER_FILE = $(SRC_PATH)/project.ld

# Program name definition for ARM GNU C compiler.
CC      = ${PREFIX_ARM}-gcc
# Program name definition for ARM GNU Linker.
LD      = ${PREFIX_ARM}-ld
# Program name definition for ARM GNU Object copy.
CP      = ${PREFIX_ARM}-objcopy
# Program name definition for ARM GNU Object dump.
OD      = ${PREFIX_ARM}-objdump

# Option arguments for C compiler.
CFLAGS = -mthumb ${CPU} ${FPU} -ffunction-sections -fdata-sections -MD -std=c99 -Wall -pedantic -c -g
# Library stuff passed as flags!
CFLAGS += -I ${STELLARISWARE_PATH} -I $(INC_PATH) -DPART_$(PART) -DTARGET_IS_BLIZZARD_RA1

# Flags for LD
LFLAGS  = --gc-sections

# Objcopy
HEX  = $(CP) -O ihex
BIN  = $(CP) -O binary -S

# flags for objectdump
ODFLAGS = -S

# I want to save the path to libgcc, libc.a and libm.a for linking.
# I can get them from the gcc frontend, using some options.
# See gcc documentation
LIB_GCC_PATH=${shell ${CC} ${CFLAGS} -print-libgcc-file-name}
LIBC_PATH=${shell ${CC} ${CFLAGS} -print-file-name=libc.a}
LIBM_PATH=${shell ${CC} ${CFLAGS} -print-file-name=libm.a}

# Uploader tool path.
# Set a relative or absolute path to the upload tool program.
# I used this project: https://github.com/utzig/lm4tools
FLASHER=~/stellaris/lm4tools/lm4flash/lm4flash
# Flags for the uploader program.
FLASHER_FLAGS=


PROJ = $(BIN_PATH)/$(PROJECT_NAME)

SRC = $(wildcard $(addsuffix /*.c, $(addprefix $(SRC_PATH)/, $(SRC_DIRS)) ) )
# SRC = $(wildcard $(SRC_PATH)/*.c)
OBJS = $(patsubst $(SRC_PATH)%, $(BIN_PATH)%, $(SRC:.c=.o))
BIN_DIRS = $(addprefix $(BIN_PATH)/, $(SRC_DIRS))

all: mkdirs $(OBJS) ${PROJ}.axf ${PROJ}

$(BIN_PATH)/%.o: $(SRC_PATH)/%.c
	@echo
	@echo Compiling $<...
	$(CC) -c $(CFLAGS) ${<} -o ${@}

${PROJ}.axf: $(OBJS)
	@echo
	@echo Making driverlib
	$(MAKE) -C ${STELLARISWARE_PATH}/driverlib/
	@echo
	@echo Linking...
	$(LD) -T $(LINKER_FILE) $(LFLAGS) -o ${PROJ}.axf \
		$(OBJS) ${STELLARISWARE_PATH}/driverlib/gcc-$(DRIVER)/libdriver-$(DRIVER).a \
		$(LIBM_PATH) $(LIBC_PATH) $(LIB_GCC_PATH)

${PROJ}: ${PROJ}.axf
	@echo
	@echo Copying...
	$(BIN) ${PROJ}.axf ${PROJ}.hex
	$(HEX) ${PROJ}.axf ${PROJ}.hex
	@echo
	@echo Creating list file...
	$(OD) $(ODFLAGS) ${PROJ}.axf > ${PROJ}.lst

mkdirs:
	@echo $(OBJS)
	mkdir -p $(BIN_DIRS)

clean:
	rm -rf $(BIN_PATH)
