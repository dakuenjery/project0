#include "io/lcd.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "io/gpio.h"

inline static void lcd_pulse(const struct LCD *lcd) {
    GPIO_ON(lcd->CMD_PORT, lcd->E);
    SysCtlDelay(SysCtlClockGet()/12000); // 0.5 ms
    GPIO_OFF(lcd->CMD_PORT, lcd->E);
    SysCtlDelay(SysCtlClockGet()/12000); // 0.5 ms
}

void LCD_4bit_init(const struct LCD *lcd)
{
    GPIOPinTypeGPIOOutput(lcd->CMD_PORT, lcd->RS|lcd->RW|lcd->E);
    GPIOPinTypeGPIOOutput(lcd->DATA_PORT, lcd->DATA_PINS);

    GPIO_OFF(lcd->CMD_PORT, lcd->RS|lcd->RW|lcd->E);
    GPIO_ON(lcd->DATA_PORT, lcd->DATA_PINS);

    SysCtlDelay(SysCtlClockGet()/300); // 20 ms

    // set to 4 bit mode
    LCD_8bit_send_cmd(lcd, (MODE_4BIT >> 4));

    LCD_4bit_send_cmd(lcd, MODE_4BIT | MODE_5X10 | MODE_2STR);
    LCD_4bit_send_cmd(lcd, DISPLAY_ENABLE | DISPLAY_CURSOR_BLINK | DISPLAY_CURSOR);
}

void LCD_8bit_send_cmd(const struct LCD *lcd, char cmd) {
    GPIOPinWrite(lcd->DATA_PORT, lcd->DATA_PINS, cmd);
    lcd_pulse(lcd);
}

void LCD_4bit_send_cmd(const struct LCD *lcd, char cmd) {
    LCD_8bit_send_cmd(lcd, (cmd >> 4));
    LCD_8bit_send_cmd(lcd, cmd);
}

void LCD_4bit_write(const struct LCD *lcd, char data)
{
    GPIO_ON(lcd->DATA_PORT, lcd->RS);
    LCD_4bit_send_cmd(lcd, data);
    GPIO_OFF(lcd->DATA_PORT, lcd->RS);
}


void LCD_format_print(const struct LCD *lcd, unsigned int flgs, const char *string, ...)
{

}
