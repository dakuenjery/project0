#include "io/ioutils.h"


const char* i_to_str(unsigned int number, char base, char isSigned) {
    static const char* charDigits = "0123456789abcdef";

    unsigned int num = 1, num2 = 1;
    static char charBuf[16]; char charBufInd = 0;

    if (isSigned && (signed int)number < 0) {
        charBuf[charBufInd++] = '-';
    }

    do {
        num = num2;
        num2 *= base;
    } while (num2 <= number);

    do {
        charBuf[charBufInd++] = charDigits[(number/num)%base];
        num /= base;
    } while (num);

    charBuf[charBufInd] = '\0';

    return charBuf;
}

const char *int_to_str(int number)
{
    return i_to_str(number, 10, 1);
}


const char *uint_to_str(unsigned int number)
{
    return i_to_str(number, 10, 0);

}


const struct data *format_parse(const char **pString, va_list args)
{
    unsigned int i = 0;
    static struct data string = {0, 0};
    while (*(*pString) != '\0') {
        if (*(*pString) == '%') {
            ++(*pString);
            switch (*(*pString)++) {
            case 'i':
            case 'd': {
                string.data = i_to_str(va_arg(args, unsigned int), 10, 1);
                return &string;
            }
            case 'u': {
                string.data = i_to_str(va_arg(args, unsigned int), 10, 0);
                return &string;
            }
            case 'x':
            case 'p': {
                string.data = i_to_str(va_arg(args, unsigned int), 16, 0);
                return &string;
            }
            case 's': {
                string.data = va_arg(args, char*);
                return &string;
            }
            default: {
                string.data = " -- % err -- ";
                return &string;
            }
            }
        }

        for (i = 0; ((*pString)[i] != '%') && ((*pString)[i] != '\0'); ++i);

        string.data = (*pString);
        string.size = i;

        (*pString) += i;

        return &string;
    }

    return 0;
}
