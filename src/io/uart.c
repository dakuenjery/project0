#include "io/uart.h"
#include "io/gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/uart.h"
#include "driverlib/sysctl.h"
#include "io/ioutils.h"
#include "string.h"
#include "stdarg.h"


void UART_init(struct UART *uart, unsigned int ulBaud) {
    SysCtlPeripheralEnable(uart->periph);
    GPIOPinConfigure(uart->rx_pin);
    GPIOPinConfigure(uart->tx_pin);
    GPIOPinTypeUART(uart->port, uart->rx_pin | uart->tx_pin);
    UARTConfigSetExpClk(uart->base, SysCtlClockGet(), ulBaud,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                         UART_CONFIG_PAR_NONE));
}


int UART_send(struct UART *uart, unsigned char* string) {
    int ind = 0;
    for (ind = 0; string[ind] != '\0'; ++ind) {
        UARTCharPut(uart->base, string[ind]);
    }
    return ind;
}

int UART_write(struct UART *uart, unsigned char* string, int len) {
    int ind;
    for (ind = 0; string[ind] != '\0' && ind < len; ++ind) {
        UARTCharPut(uart->base, string[ind]);
    }
    return ind;
}

int UART_printf(const struct UART *uart, const char *pString, ...) {
    int ret = 0;
    va_list vaArg;
    const struct data* string;

    va_start(vaArg, pString);

    while ( (string = format_parse(&pString, vaArg)) != 0 ) {
        if (string->size == 0) {
            ret += UART_send(uart, string->data);
        } else {
            ret += UART_write(uart, string->data, string->size);
        }
    }

    va_end(vaArg);
    return ret;
}

unsigned int UART_get(struct UART *uart, unsigned char* buffer, unsigned char stop, unsigned int buffer_size) {
    char ch = 0;
    unsigned int count = 0;

    for (; buffer[count] != stop || ch != stop || count < buffer_size; ++count) {
        ch = UARTCharGet(uart->base);
        buffer[count] = ch;
    }

    return count;
}
