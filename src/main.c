#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/lm3s328.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/uart.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "io/uart.h"
#include "io/gpio.h"
#include "io/lcd.h"
#include "io/ioutils.h"
#include "utils.h"
#include "string.h"

struct UART uart_io = {
    SYSCTL_PERIPH_UART0,
    U0RX_PORT, U0RX_PIN, U0TX_PIN, UART0_BASE
};

struct LCD lcd0 = {
    GPIO_PORTB_BASE, PIN4, PIN5, PIN6,
    GPIO_PORTB_BASE, (PIN0|PIN1|PIN2|PIN3)
};

#define BUFFER_SIZE (80)
unsigned char uart_buffer[BUFFER_SIZE];


int main(void)
{   
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, PIN2|PIN3);

    UART_init(&uart_io, 115200);
    LCD_4bit_init(&lcd0);

    UART_send(&uart_io, "!");
//    UART_printf(&uart_io, "UART string\r\n");
    UART_printf(&uart_io, " : clock = %u \n\r", SysCtlClockGet());
    
    for (int q = 0; q < 16; ++q) {
        LCD_4bit_write(&lcd0, "Cortex lm3s328!    "[q]);
    }

    LCD_4bit_send_cmd(&lcd0, LINE_2);

    const char* i_str = i_to_str(0x12f400, 16, 1);
    for (; *i_str != '\0'; i_str++) {
        LCD_4bit_write(&lcd0, *i_str);
    }

    for (unsigned int i = 0; 1; ++i)
    {
        GPIO_TOGGLE(GPIO_PORTA_BASE, GPIO_PIN_2|GPIO_PIN_3);
        SysCtlDelay(SysCtlClockGet()/60);
    }
}
